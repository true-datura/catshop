from rest_framework.filters import BaseFilterBackend

from .serializers import UserListQuerySerializer


class CurrentUserFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        serializer = UserListQuerySerializer(data=request.query_params)

        serializer.is_valid(raise_exception=True)

        if 'user' in serializer.validated_data:
            return queryset.filter(user=serializer.validated_data['user'])
        else:
            return queryset.filter(user=request.user)

    def get_fields(self, view):
        return ['user']