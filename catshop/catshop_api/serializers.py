import stripe
from .models import UserProfile, UserImage, UserTask
from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField



class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserImage
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserProfile
        fields = '__all__'


class UserTaskSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = UserTask
        fields = '__all__'

    def create(self, validated_data):
        stripe.api_key = "sk_test_BQokikJOvBiI2HlWgH4olfQ2"
        try:
            response = stripe.Charge.create(
                            amount=validated_data['image_counter'] * 100,  # Amount in cents
                            currency="usd",
                            source=self.context['request']._data['token'],
                            description="Example charge"
                        )
            print(response)
            if response['status'] == 'succeeded':
                print(validated_data)
                task = UserTask(
                    user=validated_data['user'],
                    image_counter=validated_data['image_counter'],
                    image_filter=validated_data['image_filter']
                )
                task.save()
                return task

        except stripe.error.CardError as e:
            # The card has been declined
            pass


class UserSerializer(serializers.ModelSerializer):
    userprofile = UserProfileSerializer(read_only=True)
    image = ImageSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = '__all__'


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'email',)

    def create(self, validated_data):
        user = User(email=validated_data['email'], username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return user


class UserListQuerySerializer(serializers.Serializer):
    user = PrimaryKeyRelatedField(queryset=User.objects.all(), required=False)
