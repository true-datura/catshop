from django.apps import AppConfig


class CatshopApiConfig(AppConfig):
    name = 'catshop_api'
