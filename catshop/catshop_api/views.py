from rest_framework.response import Response
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.filters import OrderingFilter
from django.contrib.auth.models import User
from catshop_api.models import UserProfile, UserTask, UserImage
from catshop_api.serializers import UserSerializer, UserProfileSerializer, ImageSerializer, UserTaskSerializer, UserCreateSerializer
from .filters import CurrentUserFilter

# class ExampleView(APIView):
#     authentication_classes = (SessionAuthentication, BasicAuthentication)
#     permission_classes = (IsAuthenticated,)
#
#     def get(self, request, format=None):
#         content = {
#             'user': unicode(request.user),  # `django.contrib.auth.User` instance.
#             'auth': unicode(request.auth),  # None
#         }
#         return Response(content)


class CreateUser(generics.CreateAPIView):
    permission_classes = ()
    serializer_class = UserCreateSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer


class ImageViewSet(viewsets.ModelViewSet):
    queryset = UserImage.objects.all()
    serializer_class = ImageSerializer


class UserTaskViewSet(viewsets.ModelViewSet):
    filter_backends = (CurrentUserFilter, OrderingFilter)
    serializer_class = UserTaskSerializer
    ordering = '-id'
    queryset = UserTask.objects.all()
