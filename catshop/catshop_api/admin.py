from django.contrib import admin
from .models import UserProfile, UserImage, UserTask


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ('user',)
    empty_value_display = 'Empty'
    fields = ('user',)


@admin.register(UserImage)
class UserImageAdmin(admin.ModelAdmin):
    list_display = ('user', 'image', 'informed',)
    empty_value_display = 'Empty'
    fields = ('user', 'image', 'informed',)


@admin.register(UserTask)
class UserTaskAdmin(admin.ModelAdmin):
    list_display = ('user', 'image_counter', 'image_filter',)
    empty_value_display = 'Empty'
    fields = ('user', 'image_counter', 'image_filter',)
