from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


class UserProfile(models.Model):
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        primary_key=True,
    )


class UserTask(models.Model):
    user = models.ForeignKey(User)
    image_counter = models.IntegerField(null=True)
    image_filter = models.TextField(max_length=20)


class UserImage(models.Model):
    user = models.ForeignKey(User, related_name='image')
    informed = models.BooleanField(default=False)
    image = models.ImageField(upload_to='user_images', blank=True)


@receiver(post_save, sender=User)
def create_userprofile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)