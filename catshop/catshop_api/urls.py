from django.conf.urls import url, include
from rest_framework import routers
from . import  views
from rest_framework.authtoken.views import obtain_auth_token
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'user-profiles', views.UserProfileViewSet)
router.register(r'user-tasks', views.UserTaskViewSet, 'Tasks')
router.register(r'images', views.ImageViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^register/', views.CreateUser.as_view()),
    url(r'^api-token-auth/', obtain_auth_token),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]