import urllib.request

from PIL import Image, ImageFilter
from time import sleep
from django.core.files import File
from selenium import webdriver
from random import choice
from selenium.webdriver import ActionChains

from catshop.celery import app
from catshop_api.models import UserTask, UserImage

ALLOWED_FORMATS = ('jpg', 'gif', 'png', 'bmp',)

FILTERS = {
    'blur': ImageFilter.BLUR,
    'contour': ImageFilter.CONTOUR,
    'detail': ImageFilter.DETAIL,
    'edge': ImageFilter.EDGE_ENHANCE_MORE,
    'emboss': ImageFilter.EMBOSS,
    'smooth': ImageFilter.SMOOTH_MORE,
    'sharpen': ImageFilter.SHARPEN,
}


@app.task
def meta_all_tasks(*args):
    tasks = UserTask.objects.filter(image_counter__gt=0)
    for task in tasks:
        pk = task.user.pk
        (fetch_image.s() | process_image.s(pk) | submit.s(pk)).apply_async()


@app.task(bind=True, max_retries=3)
def fetch_image(self):
    driver = webdriver.Chrome()
    actionChains = ActionChains(driver)
    driver.get('https://www.google.com.ua/search?q=cat&tbm=isch')
    thumbs = driver.find_elements_by_css_selector('.rg_ic')
    actionChains.click(choice(thumbs)).perform()
    sleep(1)
    image_src = driver.find_element_by_css_selector('.irc_mi').get_attribute('src')
    if not image_src or image_src.split('.')[-1] not in ALLOWED_FORMATS:
        driver.close()
        raise self.retry(countdown=5)
    driver.close()
    return image_src


@app.task(bind=True)
def process_image(self, image_src, pk):
    image_filter = UserTask.objects.get(user__pk=pk).image_filter
    image_name = image_src.split('/')[-1]
    urllib.request.urlretrieve(image_src, image_name)
    processed_image = Image.open(image_name).convert('RGB')
    processed_image.filter(FILTERS[image_filter]).save(image_name)
    return image_name


@app.task(bind=True)
def submit(self, image_name, pk):
    img = UserImage(
        user_id=pk,
    )
    img.image.save(image_name, File(open(image_name, 'rb')), save=True)
    task = UserTask.objects.get(pk=pk)
    task.image_counter -= 1
    print(task.image_counter)
    task.save()