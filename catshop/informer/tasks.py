from django.core.mail import send_mail
from catshop.celery import app
from catshop_api.models import UserTask, UserImage, User


@app.task
def meta_all_tasks(*args):
    images = UserImage.objects.filter(informed=False)
    for img in images:
        user = img.user
        send_the_image.s(user.email, img.image.url).apply_async()
        img.informed = True
        img.save()


@app.task(bind=True)
def send_the_image(self, email, img_src):
    try:
        send_mail(
            'Catshop notification',
            'Here is your cat: {}'.format(img_src),
            'admin@catshop.domain',
            [email],
            fail_silently=False,
        )
    except:
        self.retry(countdown=5)