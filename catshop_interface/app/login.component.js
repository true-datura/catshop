"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var api_service_1 = require('./api.service');
var LoginComponent = (function () {
    function LoginComponent(apiService, router) {
        this.apiService = apiService;
        this.router = router;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function (username, password) {
        var _this = this;
        this.apiService
            .getToken(username, password)
            .then(function (res) {
            _this.token = res;
            _this.apiService.updateHeaders(_this.token);
            //console.log(this.token);
            //console.log(this.apiService.isLoggedIn())
            _this.router.navigate(['create']);
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'my-task-login',
            template: "\n    <form class=\"form-signin\">\n      <label for=\"inputUsername\" class=\"sr-only\">Email address</label>\n      <input type=\"email\" id=\"inputUsername\" class=\"form-control\" placeholder=\"Username\" required autofocus #username>\n      <label for=\"inputPassword\" class=\"sr-only\">Password</label>\n      <input type=\"password\" id=\"inputPassword\" class=\"form-control\" placeholder=\"Password\" required #password>\n      <a class=\"btn btn-lg btn-primary btn-block\" (click)=\"login(username.value, password.value)\">Sign in</a>\n    </form>\n    ",
            styleUrls: ['login.component.css'],
            moduleId: module.id
        }), 
        __metadata('design:paramtypes', [api_service_1.ApiService, router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map