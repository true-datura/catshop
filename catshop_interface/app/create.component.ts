import { Component, Input, OnInit } from '@angular/core';
import { Router }   from '@angular/router';
import { Location }                 from '@angular/common';

import { ApiService } from './api.service';
import { Task } from './task';

@Component({
    selector: 'my-task-create',
    template: `
      <h3>Purchase task:</h3>
      <label>How many images you want: </label>
      <input [(ngModel)]="task.image_counter" placeholder="0"/>
      <label>Filter? </label>
      <select [(ngModel)]="task.image_filter">
        <option>blur</option>
        <option>contour</option>
        <option>emboss</option>
      </select>
      <button (click)="goToPurchase()">Save</button>
    `
})
export class TaskCreationComponent implements OnInit{
  task = new Task();
  stripe_data: string;
  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}
  ngOnInit(): void {
    if (this.apiService.isLoggedIn()) {
      this.task.image_filter = 'blur';
      this.task.image_counter = 1
    }
    else {
      this.router.navigate(['login'])
    }
  }
  goToPurchase(): void {
    this.apiService.image_counter = this.task.image_counter;
    this.apiService.image_filter = this.task.image_filter;
    this.router.navigate(['purchase'])
  }
}