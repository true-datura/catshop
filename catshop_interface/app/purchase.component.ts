import { Component, Input, OnInit } from '@angular/core';
import { Router }   from '@angular/router';
import { Location }                 from '@angular/common';

import { ApiService } from './api.service';
import { Task } from './task';

@Component({
    selector: 'my-task-purchase',
    template: `
    <h3>Purchase task:</h3>
    <form id="payment-form">
      <span class="payment-errors"></span>

      <div class="form-row">
        <label>
          <span>Card Number</span>
          <input type="text" size="20" data-stripe="number" #card_number>
        </label>
      </div>

      <div class="form-row">
        <label>
          <span>Expiration (MM/YY)</span>
          <input type="text" size="2" data-stripe="exp_month" #exp_month>
        </label>
        <span> / </span>
        <input type="text" size="2" data-stripe="exp_year" #exp_year>
      </div>

      <div class="form-row">
        <label>
          <span>CVC</span>
          <input type="text" size="4" data-stripe="cvc" #cvc>
        </label>
      </div>

      <div class="form-row">
        <label>
          <span>Billing Zip</span>
          <input type="text" size="6" data-stripe="address_zip" #adress_zip>
        </label>
      </div>

      <a class="btn btn-primary"
      (click)="sendOrderInfo(card_number.value, exp_month.value, exp_year.value, cvc.value, adress_zip.value)">
      Submit Payment
      </a>
    </form>
    `
})
export class TaskPurchaseComponent implements OnInit{
  stripe_token: string;
  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}
  ngOnInit(): void {
    if (this.apiService.isLoggedIn()) {
    }
    else {
      this.router.navigate(['login'])
    }
  }
  sendOrderInfo(card_number: string, exp_month: string, exp_year: string, cvc: string, adress_zip: string): void {
    var handler = (<any>window).Stripe.card.createToken({
      number: card_number,
      cvc: cvc,
      exp_month: exp_month,
      exp_year: exp_year,
      address_zip: adress_zip 
    }, (resp, info) => {
      this.apiService
        .createTask(
          this.apiService.image_counter,
          this.apiService.image_filter,
          info.id
         )
        .then(() => this.router.navigate(['create']));
    });

  }
}