"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var ApiService = (function () {
    function ApiService(http) {
        this.http = http;
        this.castshopApiUrl = 'http://127.0.0.1:8000'; // URL to web api
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
    }
    ApiService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    ApiService.prototype.getTasks = function () {
        var url = this.castshopApiUrl + "/user-tasks/";
        console.log(url);
        return this.http
            .get(url, { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.createTask = function (image_counter, filter, token) {
        var url = this.castshopApiUrl + "/user-tasks/";
        return this.http
            .post(url, JSON.stringify({
            image_counter: image_counter,
            image_filter: filter,
            token: token
        }), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ApiService.prototype.getToken = function (username, password) {
        var _this = this;
        var url = this.castshopApiUrl + "/api-token-auth/";
        return this.http
            .post(url, JSON.stringify({
            username: username,
            password: password,
        }), { headers: this.headers })
            .toPromise()
            .then(function (res) { return _this.token = res.json().token; })
            .catch(this.handleError);
    };
    ApiService.prototype.updateHeaders = function (token) {
        //console.log(token)
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json', 'Authorization': 'Token ' + token });
        //console.log(this.headers)
    };
    ApiService.prototype.isLoggedIn = function () {
        if (this.token) {
            return true;
        }
        else {
            return false;
        }
    };
    ApiService.prototype.logOut = function () {
        this.headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        this.token = '';
    };
    ApiService.prototype.register = function (username, password, email, first_name) {
        var url = this.castshopApiUrl + "/register/";
        return this.http
            .post(url, JSON.stringify({
            username: username,
            password: password,
            email: email,
            first_name: first_name
        }), { headers: this.headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(this.handleError);
    };
    ApiService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], ApiService);
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=api.service.js.map