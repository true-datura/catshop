import { Component, Input, OnInit } from '@angular/core';
import { Router }   from '@angular/router';
import { ApiService } from './api.service';

@Component({
    selector: 'my-task-login',
    template: `
    <form class="form-signin">
      <label for="inputUsername" class="sr-only">Email address</label>
      <input type="email" id="inputUsername" class="form-control" placeholder="Username" required autofocus #username>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Password" required #password>
      <a class="btn btn-lg btn-primary btn-block" (click)="login(username.value, password.value)">Sign in</a>
    </form>
    `,
    styleUrls: ['login.component.css'],
    moduleId: module.id
})
export class LoginComponent implements OnInit{
  token: string;
  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}
  ngOnInit(): void {
  }
  login(username: string, password: string): void {
    this.apiService
      .getToken(username, password)
      .then(res => {
        this.token = res;
        this.apiService.updateHeaders(this.token);
        //console.log(this.token);
        //console.log(this.apiService.isLoggedIn())
        this.router.navigate(['create'])
      });
    }
}
