"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var api_service_1 = require('./api.service');
var task_1 = require('./task');
var TaskCreationComponent = (function () {
    function TaskCreationComponent(apiService, router) {
        this.apiService = apiService;
        this.router = router;
        this.task = new task_1.Task();
    }
    TaskCreationComponent.prototype.ngOnInit = function () {
        if (this.apiService.isLoggedIn()) {
            this.task.image_filter = 'blur';
            this.task.image_counter = 1;
        }
        else {
            this.router.navigate(['login']);
        }
    };
    TaskCreationComponent.prototype.goToPurchase = function () {
        this.apiService.image_counter = this.task.image_counter;
        this.apiService.image_filter = this.task.image_filter;
        this.router.navigate(['purchase']);
    };
    TaskCreationComponent = __decorate([
        core_1.Component({
            selector: 'my-task-create',
            template: "\n      <h3>Purchase task:</h3>\n      <label>How many images you want: </label>\n      <input [(ngModel)]=\"task.image_counter\" placeholder=\"0\"/>\n      <label>Filter? </label>\n      <select [(ngModel)]=\"task.image_filter\">\n        <option>blur</option>\n        <option>contour</option>\n        <option>emboss</option>\n      </select>\n      <button (click)=\"goToPurchase()\">Save</button>\n    "
        }), 
        __metadata('design:paramtypes', [api_service_1.ApiService, router_1.Router])
    ], TaskCreationComponent);
    return TaskCreationComponent;
}());
exports.TaskCreationComponent = TaskCreationComponent;
//# sourceMappingURL=create.component.js.map