import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TaskDetailComponent } from './tasks.component';
import { AppComponent } from './app.component';
import { TaskCreationComponent } from './create.component';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { TaskPurchaseComponent } from './purchase.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'tasks',
    component: TaskDetailComponent
  },
  {
    path: 'create',
    component: TaskCreationComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'purchase',
    component: TaskPurchaseComponent
  }
];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);