import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Task } from './task'

@Injectable()
export class ApiService {

  constructor(private http: Http) { }
  image_counter: number;
  image_filter: string;
  private castshopApiUrl = 'http://127.0.0.1:8000';  // URL to web api
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
  private headers = new Headers({'Content-Type': 'application/json'});
  private token: string;
  getTasks(): Promise<Task[]> {
    const url = `${this.castshopApiUrl}/user-tasks/`;
    console.log(url);
    return this.http
      .get(url, {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Task[])
      .catch(this.handleError);
  }
  createTask(image_counter: number, filter: string, token: string): Promise<Task> {
    const url = `${this.castshopApiUrl}/user-tasks/`;
    return this.http
      .post(url, JSON.stringify(
        {
          image_counter: image_counter,
          image_filter: filter,
          token: token
        }),
        {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Task)
      .catch(this.handleError);
  }
  getToken(username: string, password: string): Promise<string> {
    const url = `${this.castshopApiUrl}/api-token-auth/`;
    return this.http
      .post(url, JSON.stringify(
        {
          username: username,
          password: password,
        }),
        {headers: this.headers})
      .toPromise()
      .then(res => this.token = res.json().token)
      .catch(this.handleError);
  }
  updateHeaders(token: string): void {
    //console.log(token)
    this.headers = new Headers({'Content-Type': 'application/json', 'Authorization': 'Token ' + token});
    //console.log(this.headers)
  }
  isLoggedIn(): boolean {
    if (this.token) {
      return true;
    }
    else {
      return false;
    }
  }
  logOut(): void {
    this.headers = new Headers({'Content-Type': 'application/json'});
    this.token = '';
  }
  register(username: string, password: string, email: string, first_name: string): Promise<string> {
    const url = `${this.castshopApiUrl}/register/`;
    return this.http
      .post(url, JSON.stringify(
        {
          username: username,
          password: password,
          email: email,
          first_name: first_name
        }),
        {headers: this.headers})
      .toPromise()
      .then(res => res.json())
      .catch(this.handleError);
  }  
}