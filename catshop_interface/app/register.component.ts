import { Component, Input, OnInit } from '@angular/core';
import { Router }   from '@angular/router';
import { ApiService } from './api.service';

@Component({
    selector: 'my-task-register',
    templateUrl: 'register.component.html',
    styleUrls: ['register.component.css'],
    moduleId: module.id
})
export class RegisterComponent implements OnInit{
  constructor(
    private apiService: ApiService,
    private router: Router
  ) {}
  ngOnInit(): void {
  }
  register(username: string, password: string, email: string, first_name: string): void {
    this.apiService
      .register(username, password, email, first_name)
      .then(() => this.router.navigate(['login']));
  }
}