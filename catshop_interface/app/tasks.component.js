"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var common_1 = require('@angular/common');
var api_service_1 = require('./api.service');
var TaskDetailComponent = (function () {
    function TaskDetailComponent(apiService, route, location, router) {
        this.apiService = apiService;
        this.route = route;
        this.location = location;
        this.router = router;
    }
    TaskDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log('Logged: ', this.apiService.isLoggedIn());
        if (this.apiService.isLoggedIn() == false) {
            this.router.navigate(['login']);
        }
        ;
        this.apiService
            .getTasks()
            .then(function (res) { return _this.tasks = res; });
    };
    TaskDetailComponent.prototype.goBack = function () {
        this.location.back();
    };
    TaskDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-task-detail',
            template: "\n    <h3>Your tasks:</h3>\n    <table class=\"table table-bordered\">\n    <thead>\n      <tr>\n        <th>Task id</th>\n        <th>Images to scrap</th>\n        <th>Filter per this task</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let task of tasks\">\n        <td>{{task.id}}</td>\n        <td>{{task.image_counter}}</td>\n        <td>{{task.image_filter}}</td>\n      </tr>\n    </tbody>\n    </table>\n    "
        }), 
        __metadata('design:paramtypes', [api_service_1.ApiService, router_1.ActivatedRoute, common_1.Location, router_1.Router])
    ], TaskDetailComponent);
    return TaskDetailComponent;
}());
exports.TaskDetailComponent = TaskDetailComponent;
//# sourceMappingURL=tasks.component.js.map