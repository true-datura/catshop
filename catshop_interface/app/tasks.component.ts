import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router }   from '@angular/router';
import { Location }                 from '@angular/common';

import { ApiService } from './api.service';
import { Task } from './task';
@Component({
    selector: 'my-task-detail',
    template: `
    <h3>Your tasks:</h3>
    <table class="table table-bordered">
    <thead>
      <tr>
        <th>Task id</th>
        <th>Images to scrap</th>
        <th>Filter per this task</th>
      </tr>
    </thead>
    <tbody>
      <tr *ngFor="let task of tasks">
        <td>{{task.id}}</td>
        <td>{{task.image_counter}}</td>
        <td>{{task.image_filter}}</td>
      </tr>
    </tbody>
    </table>
    `
})
export class TaskDetailComponent implements OnInit{
  tasks: Task[];

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {}

  ngOnInit(): void {
    console.log('Logged: ', this.apiService.isLoggedIn())
    if (this.apiService.isLoggedIn() == false) {
      this.router.navigate(['login'])
    };
    this.apiService
      .getTasks()
      .then(res => this.tasks = res);
  }
  goBack(): void {
    this.location.back();
  }
}