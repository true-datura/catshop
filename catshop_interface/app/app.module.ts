import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { routing }              from './app.routing';

import { ApiService }          from './api.service';

import { TaskDetailComponent } from './tasks.component';
import { AppComponent } from './app.component';
import { TaskCreationComponent } from './create.component';
import { LoginComponent } from './login.component';
import { RegisterComponent } from './register.component';
import { TaskPurchaseComponent } from './purchase.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    TaskDetailComponent,
    TaskCreationComponent,
    LoginComponent,
    RegisterComponent,
    TaskPurchaseComponent,
  ],
  providers: [
    ApiService,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }