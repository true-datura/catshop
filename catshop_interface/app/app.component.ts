import { Component } from '@angular/core';
import { ApiService } from './api.service';
@Component({
  selector: 'my-app',
  templateUrl: 'app.component.html',
  moduleId: module.id
})
export class AppComponent {
  constructor (
    private apiService: ApiService
  ) {}
  title = 'Catshop';
}