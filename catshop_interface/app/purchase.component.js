"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var api_service_1 = require('./api.service');
var TaskPurchaseComponent = (function () {
    function TaskPurchaseComponent(apiService, router) {
        this.apiService = apiService;
        this.router = router;
    }
    TaskPurchaseComponent.prototype.ngOnInit = function () {
        if (this.apiService.isLoggedIn()) {
        }
        else {
            this.router.navigate(['login']);
        }
    };
    TaskPurchaseComponent.prototype.sendOrderInfo = function (card_number, exp_month, exp_year, cvc, adress_zip) {
        var _this = this;
        var handler = window.Stripe.card.createToken({
            number: card_number,
            cvc: cvc,
            exp_month: exp_month,
            exp_year: exp_year,
            address_zip: adress_zip
        }, function (resp, info) {
            _this.apiService
                .createTask(_this.apiService.image_counter, _this.apiService.image_filter, info.id)
                .then(function () { return _this.router.navigate(['create']); });
        });
    };
    TaskPurchaseComponent = __decorate([
        core_1.Component({
            selector: 'my-task-purchase',
            template: "\n    <h3>Purchase task:</h3>\n    <form id=\"payment-form\">\n      <span class=\"payment-errors\"></span>\n\n      <div class=\"form-row\">\n        <label>\n          <span>Card Number</span>\n          <input type=\"text\" size=\"20\" data-stripe=\"number\" #card_number>\n        </label>\n      </div>\n\n      <div class=\"form-row\">\n        <label>\n          <span>Expiration (MM/YY)</span>\n          <input type=\"text\" size=\"2\" data-stripe=\"exp_month\" #exp_month>\n        </label>\n        <span> / </span>\n        <input type=\"text\" size=\"2\" data-stripe=\"exp_year\" #exp_year>\n      </div>\n\n      <div class=\"form-row\">\n        <label>\n          <span>CVC</span>\n          <input type=\"text\" size=\"4\" data-stripe=\"cvc\" #cvc>\n        </label>\n      </div>\n\n      <div class=\"form-row\">\n        <label>\n          <span>Billing Zip</span>\n          <input type=\"text\" size=\"6\" data-stripe=\"address_zip\" #adress_zip>\n        </label>\n      </div>\n\n      <a class=\"btn btn-primary\"\n      (click)=\"sendOrderInfo(card_number.value, exp_month.value, exp_year.value, cvc.value, adress_zip.value)\">\n      Submit Payment\n      </a>\n    </form>\n    "
        }), 
        __metadata('design:paramtypes', [api_service_1.ApiService, router_1.Router])
    ], TaskPurchaseComponent);
    return TaskPurchaseComponent;
}());
exports.TaskPurchaseComponent = TaskPurchaseComponent;
//# sourceMappingURL=purchase.component.js.map