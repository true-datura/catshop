"use strict";
var router_1 = require('@angular/router');
var tasks_component_1 = require('./tasks.component');
var create_component_1 = require('./create.component');
var login_component_1 = require('./login.component');
var register_component_1 = require('./register.component');
var purchase_component_1 = require('./purchase.component');
var appRoutes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'tasks',
        component: tasks_component_1.TaskDetailComponent
    },
    {
        path: 'create',
        component: create_component_1.TaskCreationComponent
    },
    {
        path: 'login',
        component: login_component_1.LoginComponent
    },
    {
        path: 'register',
        component: register_component_1.RegisterComponent
    },
    {
        path: 'purchase',
        component: purchase_component_1.TaskPurchaseComponent
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map